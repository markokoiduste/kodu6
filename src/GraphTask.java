import sun.awt.image.ImageWatched;

import java.util.*;

public class GraphTask {

   /**
    * Main function of the class
    * @param args
    */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
      //throw new RuntimeException ("Nothing implemented yet!"); // delete this
   }

   /**
    * Method that runs the GraphTask.
    * Method calls other methods meant for solving the task or
    * testing the task.
    */
   public void run() {
      //this.dynamicHighestRouteTester();
      this.testRun();
      //this.testLimits();
   }

   /**
    * Vertex class. Describes the structure of a Vertex
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      private int height = 0;

      /**
       * Class constructor
       * @param s name of the Vertex
       * @param v next Vertex
       * @param e first Arc of the vertex
       */
      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
         height = (int)(Math.random()*100); //I give random heights to our geographical Vertexes just to simplify my task
      }

      /**
       * Empty class constructor
       * @param s name of the Vertex
       */
      Vertex (String s) {
         this (s, null, null);
      }

      /**
       * @return String representation of the Vertex
       */
      @Override
      public String toString() {
         return id+"("+Integer.toString(height)+")";
      }
   }

   /**
    * Arc class. Describes the structure of an Arc.
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;

      /**
       * Class constructor
       * @param s name of the arc
       * @param v first vertex of the arc
       * @param a next arc on current vertex
       */
      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      /**
       * Empty class constructor
       * NB! Arc still needs a name
       * @param s name of the arc
       */
      Arc (String s) {
         this (s, null, null);
      }

      /**
       * @return String representation of the Arc
       */
      @Override
      public String toString() {
         return id;
      }
   }

   /**
    * Graph class. Contains Vertexes, Arcs and
    * multiple functions regarding graphs.
    */
   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;

      /**
       * Class constructor.
       * @param s name of the graph.
       * @param v first vertex in a graph(highest peak of a "tree")
       */
      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      /**
       * Empty class constructor. NB! Graph still needs to be named.
       * @param s name of the graph.
       */
      Graph (String s) {
         this (s, null);
      }

      /**
       * Override of .toString() function
       * @return String representation of the Graph.
       */
      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      /**
       * Method to create a Vertex in the graph.
       */
      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      /**
       * Method to create an Arc in the graph.
       */
      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Case a method gets Vertexes given by their id.
       * @param locationID String of location vertex id
       * @param destinationID String of destination vertex id
       * @return
       */
      public LinkedList<LinkedList<Vertex>> pathFinder(String locationID, String destinationID){
         return pathFinder(giveVertexByID(locationID), giveVertexByID(destinationID));
      }

      /**
       * Finds all paths between two Vertexes
       * @param locationVertex string of location-vertex
       * @param destinationVertex string of destination-vertex
       */
      public LinkedList<LinkedList<Vertex>> pathFinder(Vertex locationVertex, Vertex destinationVertex) {
         if (locationVertex.equals(destinationVertex)) {
            throw new RuntimeException("There cannot be any traversal from Vertex to itself.");
         }
         LinkedList<Vertex> path = new LinkedList<Vertex>();
         LinkedList<LinkedList<Vertex>> pathList = new LinkedList<LinkedList<Vertex>>();
         LinkedList<Arc> queue = new LinkedList<Arc>();

         Vertex v = locationVertex;
         boolean somethingChanged = true;

         path.addLast(v);
         pathList.addLast(path);

         while (somethingChanged) {
            somethingChanged = false;
            int currentPathCount = pathList.size();
            for (int i = 0; i<currentPathCount; i++) {
               path = pathList.removeLast();
               v = path.getLast();

               Arc a = v.first;

               //Building queue for current vertex
               queue.clear();
               while(a != null) {
                  queue.addLast(a);
                  a = a.next;
               }

               while (!queue.isEmpty()) {
                  a = queue.removeLast();

                  if (!path.contains(a.target) && !path.getLast().equals(destinationVertex)) {
                     path.addLast(a.target);
                     if (!pathList.contains(path)) {
                        pathList.addFirst((LinkedList<Vertex>)path.clone());
                        somethingChanged = true;
                     }
                     path.removeLast();
                  } else {
                     if (!pathList.contains(path)) {
                        pathList.addFirst((LinkedList<Vertex>) path.clone());
                     }
                  }

               }
            }
         } //All possible paths from Starting to Ending point built

         //Gonna find all paths that end in our destination.
         Iterator<LinkedList<Vertex>> iter = pathList.iterator();
         while (iter.hasNext()) {
            if (!iter.next().getLast().equals(destinationVertex)) {
               iter.remove();
            }
         }
         return pathList;
      }

      /**
       * Finds a Vertex by given id.
       * @param id id of the vertex
       * @return returns Vertex or null, depends if it exists in graph or not
       */
      public Vertex giveVertexByID(String id) {
         Vertex v = this.first;
         while(v!=null) {
            if (v.id.equals(id)) {return v;}
            v = v.next;
         }
         return null;
      }

      public LinkedList<LinkedList<Vertex>> highestRoute(String locationID, String destinationID) {
         return highestRoute(giveVertexByID(locationID),giveVertexByID(destinationID));
      }

      /**
       * Solves the task. Finds highest possible routes between given vertexes.
       * Highest route means a route that goes through the highest peak between the vertexes.
       * @param locationVertex location vertex
       * @param destinationVertex destination vertex
       */
      public LinkedList<LinkedList<Vertex>> highestRoute(Vertex locationVertex, Vertex destinationVertex) {
         LinkedList<LinkedList<Vertex>> pathList = pathFinder(locationVertex, destinationVertex);
         Iterator<LinkedList<Vertex>> iter = pathList.iterator();

         Vertex highest = new Vertex("v_test");
         highest.height = Integer.MIN_VALUE;
         Vertex current;

         while(iter.hasNext()) {
            Iterator<Vertex> iter2 = iter.next().iterator();
            while (iter2.hasNext()) {
               current = iter2.next();
               if (current.height > highest.height) {
                  highest = current;
               }
            }
         }

         iter = pathList.iterator();

         while(iter.hasNext()) {
            if (!iter.next().contains(highest)) {
               iter.remove();
            }
         }

         return pathList;
      }
   }

   /**
    * Method for dynamic testing. When run it asks necessary parameters
    * from the user.
    */
   public void dynamicHighestRouteTester() {
      Scanner input = new Scanner(System.in);
      LinkedList<String> inputList = new LinkedList<String>();
      Graph g = new Graph("G");

      System.out.print("Going to generate a graph, insert number of vertexes in a graph: ");
      inputList.addLast(input.nextLine());
      System.out.print("Insert number of arcs in generated graph: ");
      inputList.addLast(input.nextLine());

      System.out.println("Generating graph...");
      g.createRandomSimpleGraph(Integer.valueOf(inputList.removeFirst()), Integer.valueOf(inputList.removeFirst()));
      System.out.println("Graph generated!");

      System.out.println("Loading list of vertexes...");
      Vertex v = g.first;
      int n = 0;
      while(v != null) {
         System.out.printf("%5s ", v.id);
         v = v.next;
         n++;
         if (n%10 == 0) {System.out.println();}
      }
      System.out.println("\nVertexes loaded, total of: "+n);

      System.out.println("Insert location vertex: ");
      String locationID = input.nextLine();
      System.out.println("Insert destination vertex: ");
      String destinationID = input.nextLine();

      System.out.println("Generating paths...");
      LinkedList<LinkedList<Vertex>> pathList = g.pathFinder(locationID, destinationID);
      System.out.println("Paths generated, total of: "+pathList.size());

      System.out.print("Print paths? Enter 'yes' to print: ");
      inputList.addLast(input.nextLine());
      if (inputList.getLast().equals("yes")) {
         Iterator<LinkedList<Vertex>> iter = pathList.iterator();
         while(iter.hasNext()) {
            System.out.println(iter.next());
         }
      }
      input.close();
   }

   /**
    * Automatically runs a test on randomly generated graph,
    * locationID and destinationID.
    */
   public void testRun() {
      int random = (int)(Math.random()*8);
      int vertexes = random+2;
      int arcs = random+1;
      random =((random*(random-1))/2);
      random = (int)(Math.random()*random);
      arcs = arcs + random;
      Graph g = new Graph("G");
      g.createRandomSimpleGraph(vertexes, arcs);
      System.out.println("Graph generated: "+vertexes+" vertex(es), "+arcs+" arc(s)");

      int locationVertex = 0;
      int destinationVertex = 0;
      while (locationVertex == destinationVertex) {
         locationVertex = (int) (Math.random() * vertexes);
         destinationVertex = (int) (Math.random() * vertexes);
      }

      Vertex v = g.first;
      while (locationVertex > 0) {
         v = v.next;
         locationVertex--;
      }
      String locationID = v.id;
      v = g.first;
      while (destinationVertex > 0) {
         v = v.next;
         destinationVertex--;
      }
      String destinationID = v.id;
      System.out.println("Finding paths from vertex "+locationID+" to "+destinationID);

      LinkedList<LinkedList<Vertex>> pathList = g.highestRoute(locationID, destinationID);
      System.out.println("Paths generated, printing all possible paths.");
      Iterator iter = pathList.iterator();
      while (iter.hasNext()) {
         System.out.println(iter.next());
      }
   }

   /**
    * Method for test output of the Report
    */
   public void testLimits() {
      Iterator iter;

      System.out.println("\nTest #1");
      Graph g1 = new Graph("G1");
      g1.createRandomSimpleGraph(2,1); //Smallest possible solvable graph
      iter = g1.highestRoute("v1", "v2").iterator();
      while (iter.hasNext()) {
         System.out.println(iter.next());
      }

      System.out.println("\nTest #2");
      Graph g2 = new Graph("G2");
      g2.createRandomSimpleGraph(10,9); //10 Vertexes, connected by minimum number of arcs
      iter = g2.highestRoute("v1", "v10").iterator();
      while (iter.hasNext()) {
         System.out.println(iter.next());
      }

      System.out.println("\nTest #3");
      Graph g3 = new Graph("G3");
      g3.createRandomSimpleGraph(5,10); //5 Vertexes connected by maximum number of arcs
      iter = g3.highestRoute("v1", "v5").iterator();
      while (iter.hasNext()) {
         System.out.println(iter.next());
      }

      System.out.println("\nTest #4");
      Graph g4 = new Graph("G4");
      g4.createRandomSimpleGraph(10,15);
      iter = g4.highestRoute("v3", "v8").iterator();
      while (iter.hasNext()) {
         System.out.println(iter.next());
      }

      System.out.println("\nTest #5");
      Graph g5 = new Graph("G5");
      g5.createRandomSimpleGraph(10,15);
      iter = g5.highestRoute("v5", "v5").iterator();
      while (iter.hasNext()) {
         System.out.println(iter.next());
      }
   }
}